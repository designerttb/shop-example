import { createSlice } from "@reduxjs/toolkit";
// import productApi from '../custom-axios/productApi'

// export const getAllProduct = createAsyncThunk('product/getAllProduct', async (params, thunkApi) => {
//     const data = await productApi.getAll()
//     return data
// })

const productSlice = createSlice({
    name: 'product',
    initialState: {
        current: [],
        loading: false,
        error: ''
    },
    reducers: {
        getProduct() { },
        setProduct(state, action) {
            console.log('payload', action.payload)
            state.current = action.payload
        }
    }
    // extraReducers: {
    //     [getAllProduct.pending]: (state) => {
    //         state.loading = true
    //     },
    //     [getAllProduct.rejected]: (state, action) => {
    //         state.loading = false
    //         state.error = action.error
    //     },
    //     [getAllProduct.fulfilled]: (state, action) => {
    //         state.loading = false
    //         state.current = action.payload
    //     }
    // }
})
// const { reducer: productReducer } = productSlice
export const { getProduct, setProduct } = productSlice.actions
export default productSlice.reducer