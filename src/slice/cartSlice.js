import { createSlice } from "@reduxjs/toolkit";

const cart = createSlice({
    name: 'cart',
    initialState: [],
    reducers: {
        addItemToCart: (state, action) => {
            const updateItemId = action.payload.id
            const updateItemQuantity = action.payload.quantity
            const itemIndex = state.findIndex(item => item.id === updateItemId)
            console.log(itemIndex, updateItemId, updateItemQuantity)
            if (itemIndex >= 0) {
                state[itemIndex].quantity++
            } else {
                state.push(action.payload)
            }

        },
        removeItemToCart: (state, action) => {
            const removeItemId = action.payload.id
            return state.filter(item => item.id != removeItemId)
        },
        updateItemQuantity: (state, action) => {
            const updateItemId = action.payload.id
            const updateItemQuantity = action.payload.quantity
            const itemIndex = state.findIndex(item => item.id === updateItemId)
            if (itemIndex >= 0) {
                state[itemIndex].quantity = updateItemQuantity
            }
        }

    }
})
const { reducer, actions } = cart
export const { addItemToCart, removeItemToCart, updateItemQuantity } = actions
export default reducer