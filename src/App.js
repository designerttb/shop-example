// import { unwrapResult } from '@reduxjs/toolkit';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { getProduct } from 'slice/productSlice';
import styled from 'styled-components';
import Cart from './components/containers/cart/index';
import CheckOut from './components/containers/checkout/index';
import DetailProduct from './components/containers/detail-products/index';
import ListProducts from './components/containers/list-products/index';
import Header from './components/header/index';
import LeftMenu from './components/menu/index';
// import { getAllProduct } from './slice/productSlice';


const Wrapper = styled.div`
display: flex;
width: 100%;
background-color: #eee;
`
function App() {
  const dispatch = useDispatch()
  const [isLoading, setIsLoading] = useState(false)
  useEffect(() => {
    const getProductData = async () => {
      setIsLoading(true)
      dispatch(getProduct())
      // const response = await dispatch(getAllProduct())
      // const productlist = unwrapResult(response)
      setIsLoading(false)
    }
    getProductData()
  }, [dispatch])
  return (
    <div className="App" style={{ height: '100%' }}>
      <Router>
        <Header />
        <Wrapper>
          <LeftMenu />
          <Wrapper>
            <Switch>
              <Route exact path='/'>
                <ListProducts isLoading={isLoading} />
              </Route>
              <Route exact path='/cart'>
                <Cart />
              </Route>
              <Route exact path='/detail-product/:id'>
                <DetailProduct />
              </Route>
              <Route exact path='/checkout'>
                <CheckOut />
              </Route>
            </Switch>
          </Wrapper>
        </Wrapper>
      </Router>
    </div >
  );
}

export default App;
