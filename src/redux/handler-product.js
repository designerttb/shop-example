import { requestGetProduct } from "custom-axios/request"
import { call, put } from 'redux-saga/effects'
import { setProduct } from "slice/productSlice"

export function* handleGetProduct(action) {
    try {
        const response = yield call(requestGetProduct)
        const data = response.data
        console.log('data', data)
        yield put(setProduct([...data]))
    }
    catch (error) {
        console.log(error)
    }
}