import { takeLatest } from 'redux-saga/effects'
import { getProduct } from "slice/productSlice";
import { handleGetProduct } from "./handler-product";

export function* watcherSaga() {
    yield takeLatest(getProduct.type, handleGetProduct)
}