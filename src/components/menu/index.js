import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Wrapper = styled.div`
    display: flex;
`
const Nav = styled.ul`
    list-style: none;
    padding-right: 40px;
    width: 200px;

`
const Item = styled.li`
    border-bottom: 1px solid #e8e8e8;
    color: #333;
    font-size: 16px;
    padding: 12px 0;
    width: 100%;
    transition: all 0.3s;
    &:hover {
        cursor: pointer;
        opacity: 0.7;
        padding-left: 20px;
    }
`
const styleLink = {
    textDecoration: 'none',
    color: '#333'
}
const listNav = [
    {
        id: 1,
        name: 'Home',
        url: ''
    },
    {
        id: 2,
        name: 'Accessories',
        url: 'accessories'
    },
    {
        id: 3,
        name: 'Jeans',
        url: 'jeans'
    },
    {
        id: 4,
        name: 'Outerwear',
        url: 'outerwear'
    },
    {
        id: 5,
        name: 'Pants',
        url: 'pants'
    },
    {
        id: 6,
        name: 'Shirts',
        url: 'shirts'
    },
    {
        id: 7,
        name: 'T - Shirts',
        url: 't-shirts'
    },
    {
        id: 8,
        name: 'Shorts',
        url: 'shorts'
    }
]

function LeftMenu(props) {
    return (
        <Wrapper>
            <Nav>
                {listNav.map((item) => <Item><Link style={styleLink} to={`/${item.url}`}>{item.name}</Link></Item>)}
            </Nav>
        </Wrapper>
    );
}

export default LeftMenu;