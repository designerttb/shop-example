import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import Slider from 'react-slick';
import styled from 'styled-components';
import { addItemToCart } from '../../../slice/cartSlice';


const Wrapper = styled.div`
    display: flex;
    flex-flow: column wrap;
`
const Detail = styled.div`
    display: flex;
    flex-flow: row wrap;
    justify-content: flex-end;

`
const WrappImage = styled.div`
    flex:1;
`
const ImageProduct = styled.img`
    max-width: 720px;
    max-height: 720px;
    border-radius: 6px;
    margin: 10px;
`
// const Form = styled.form`
//     width: 100%;
//     flex: 1;
//     margin-left: 50px;
// `
const Cart = styled.div`
    display: flex;
    flex-flow: column;
`
const Select = styled.select`
    padding: 8px;
    background-color: #f4f4f4;
    font-size: 15px;
`
const NameProduct = styled.h1`
    margin: 10px 0;
`
const PriceProduct = styled.span`
    font-size: 21px
`
const Label = styled.label`
    font-size: 12px;
    margin-bottom: 3px;

`
const Button = styled.button`
    padding: 10px;
    margin-top: 10px;
    font-size: 16px;
    text-transform: uppercase;
    background: #fff;
    border: 2px solid #333;
    border-radius: 1px;
    transition: all 0.3s;  
    &:hover {
        opacity: 0.7;
        cursor: pointer;
    } 
`
const Wrap = styled.div`
    display: flex;
    flex-flow: row;
    padding: 20px 0;
`
const WrapSelect = styled.div`
    display: flex;
    flex-flow: column;
    padding-right: 10px;
`
const Description = styled.div``

const Recommend = styled.div`
    display: block;
`
// const settings = {
//     dots: true,
//     infinite: true,
//     autoplay: true,
//     speed: 500,
//     slidesToShow: 4,
//     slidesToScroll: 1
// }

function DetailProduct({ onAddToCart }) {
    const [isActive, setIsActive] = useState(true)
    const id = useParams().id
    const history = useHistory()
    const dispatch = useDispatch()
    const productList = useSelector(state => state.product.current)
    const currentProduct = productList.find(item => item.id === id && item)
    const handleAddToCart = () => {
        const newItemCart = {
            "id": currentProduct.id,
            "src": currentProduct.src,
            "name": currentProduct.name,
            "price": currentProduct.price,
            "color": currentProduct.color,
            "quantity": currentProduct.quantity || 1,
            "size": currentProduct.size
        }
        const action = addItemToCart(newItemCart)
        dispatch(action)
        setIsActive(false)
    }
    const goAddress = (address) => {
        history.push(`/${address}`)
    }
    return (
        <Wrapper>
            <Detail>
                <WrappImage>
                    <ImageProduct src={currentProduct.src}></ImageProduct>
                </WrappImage>
                <Wrapper>
                    <NameProduct>{currentProduct.name}</NameProduct>
                    <PriceProduct>${currentProduct.price}</PriceProduct>
                    <Wrap>
                        <WrapSelect>
                            <Label>Color</Label>
                            <Select><option>Navy</option></Select>
                        </WrapSelect>
                        <WrapSelect>
                            <Label>Size</Label>
                            <Select><option>Small</option></Select>
                        </WrapSelect>
                    </Wrap>
                    <Cart>
                        <Button onClick={isActive ? handleAddToCart : goAddress('cart')}>{isActive ? 'Add to card' : 'View cart'}</Button>
                        <Button style={{ background: '#333', color: '#fff' }} onClick={() => goAddress('checkout')}>Buy it now</Button>
                    </Cart>
                </Wrapper>
            </Detail>
            <Description>{currentProduct.description}</Description>
            <Recommend>
                <Slider>

                </Slider>
            </Recommend>
        </Wrapper>
    )
}

export default DetailProduct;