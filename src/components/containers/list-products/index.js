import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import Product from '../../product/index';

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    flex: 1;
`

function ListProducts({ isLoading }) {
    const products = useSelector(state => state.product.current)
    console.log('list', products)
    return (
        <Wrapper>
            {
                isLoading ? <div><h1>Loading...&nbsp;<i class="fas fa-spinner fa-spin"></i></h1></div> : products.map((item) => <Product products={item} />)
            }
        </Wrapper>
    );
}

export default ListProducts;