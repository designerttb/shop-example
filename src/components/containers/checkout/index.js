import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import subTotal from '../../common/total'
const Wrapper = styled.div`
    display: flex;
`
const WrapInfor = styled.div`
    display: flex;
    flex-flow: column;
    flex: 1;
`
const Input = styled.input`
    padding: 10px;
    margin: 0 10px 10px 0;
`
const WrapProduct = styled.div`
    display: flex;
    flex-flow: column;
`
const Table = styled.table`
    width: 100%;
    padding:10px;
`
const Row = styled.tr`
`
const Cell = styled.th`
    min-width: 100px;
`
const Title = styled.h1`
`
const ImgProduct = styled.img`
    width: 64px;
    height: 64px;
    border-radius: 10px;
`
const NameProduct = styled.h2``
const SizeProduct = styled.span`
    width: 100px;
    text-transform: capitalize;

`
function CheckOut() {
    const cart = useSelector(state => state.cart)
    return (
        <Wrapper>
            <WrapInfor>
                <h1>Contact information</h1>
                <Input type='email' placeholder='Email or mobile phone number'></Input>
                <h1>Shipping address</h1>
                <Wrapper>
                    <Input type='text' placeholder='Firt name (optional)'></Input>
                    <Input type='text' placeholder='Last name'></Input>
                </Wrapper>
                <Input type='text' placeholder='Adress'></Input>
                <Input type='text' placeholder='Apartment, suite, etc. (optional)'></Input>
                <Input type='text' placeholder='City'></Input>
                <Wrapper>
                    <Input type='text' placeholder='Country/Region'></Input>
                    <Input type='text' placeholder='Postal code'></Input>
                </Wrapper>
                <Input type='submit' value='Continue'></Input>
            </WrapInfor>
            <Wrapper style={{ background: '#fafafa' }}>
                <Table>
                    {cart.map((item) => (
                        <Row>
                            <Cell>
                                <Wrapper>
                                    <ImgProduct src={item.src}></ImgProduct>
                                    <WrapProduct>
                                        <NameProduct>{item.name}</NameProduct>
                                        <SizeProduct>{item.color} / {item.size}</SizeProduct>
                                    </WrapProduct>
                                </Wrapper>
                            </Cell>
                            <Cell>${item.price}</Cell>
                            <Cell>${item.price * item.quantity}</Cell>
                        </Row>
                    ))}
                    <Row>
                        <Cell />
                        <Title>SubTotal: ${subTotal(cart)}</Title>
                    </Row>
                </Table>
            </Wrapper>
        </Wrapper>
    )
}

export default CheckOut