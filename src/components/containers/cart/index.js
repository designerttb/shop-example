import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { removeItemToCart, updateItemQuantity } from '../../../slice/cartSlice'
import subTotal from '../../common/total'


const Wrapper = styled.div`
    display: flex;
    width: 100%
`
const WrapProduct = styled.div`
    display: flex;
    flex-flow: column;
`
// const styleLogo = {
//     textDecoration: 'none',
//     color: '#000'
// }
const Table = styled.table`
    width: 100%
`
const Row = styled.tr``
const Cell = styled.th`
    min-width: 100px;
`
const Title = styled.h1``
const ImgProduct = styled.img`
    width: 145px;
    margin: 0 10px;
    border-radius: 6px;
`
const NameProduct = styled.h2``
const SizeProduct = styled.span`
width: 100px;
text-transform: capitalize;

`
const Button = styled.button`
    width: 150px;
    padding: 10px;
    margin-top: 10px;
    font-size: 16px;
    text-transform: uppercase;
    background: #000;
    color: #fff;
    border: none;
    border-radius: 3px;
    transition: all 0.3s;  
    &:hover {
        opacity: 0.7;
        cursor: pointer;
    } 
`
const styleLink = {
    textDecoration: 'none',
    background: '#000',
    lineHeight: '50px',
    fontSize: '16px',
    textAlign: 'center',
    color: '#fff',
    textTransform: 'uppercase',
    width: 'auto',
    margin: '10px'
}
const Quantity = styled.input`
    width: 40px;
`
function Cart() {
    const titleCart = ['Product', 'Price', 'Quantity', 'Total']
    const history = useHistory()
    const cartList = useSelector(state => state.cart)
    const dispatch = useDispatch()
    const isEmpty = !cartList.length

    const handleRemoveToCart = (id) => {
        const removeItemCart = {
            id: id
        }
        console.log(cartList)
        const action = removeItemToCart(removeItemCart)
        dispatch(action)
    }
    const handleChangeQuantity = (quantity, id) => {
        const updateQuantityItem = {
            id: id,
            quantity: quantity
        }
        console.log(cartList)
        const action = updateItemQuantity(updateQuantityItem)
        dispatch(action)
    }
    const goAddress = (address) => {
        history.push(`/${address}`)
    }
    const EmptyCart = () => (
        <div>Your cart is currently empty.<br></br>
            <Link to='/'>Continue browsing here.</Link>
        </div>
    )
    const FilledCart = ({ cartList }) => (
        <div>
            <Table>
                <Row>
                    {titleCart.map((item) => <Cell><Title>{item}</Title></Cell>)}

                </Row>
                {cartList.map((item) => (
                    <Row>
                        <Cell>
                            <Wrapper>
                                <ImgProduct src={item.src}></ImgProduct>
                                <WrapProduct>
                                    <NameProduct>{item.name}</NameProduct>
                                    <SizeProduct>{item.color} / {item.size}</SizeProduct>
                                    <Button onClick={() => handleRemoveToCart(item.id)} > Remove</Button>
                                </WrapProduct>
                            </Wrapper>
                        </Cell>
                        <Cell>${item.price}</Cell>
                        <Cell><Quantity type='number' min='1' onChange={(e) => handleChangeQuantity(e.target.value, item.id)} defaultValue={item.quantity}></Quantity></Cell>
                        <Cell>${item.price * item.quantity}</Cell>
                    </Row>
                ))}
                <Row>
                    <Cell /><Cell /><Cell />
                    <Title>SubTotal: ${subTotal(cartList)}</Title>
                </Row>
            </Table>
            <Button style={styleLink} onClick={() => goAddress('')}>Continue Shopping</Button>
            <Button style={styleLink} onClick={() => goAddress('checkout')}>Check Out</Button>
        </div>
    )
    return (
        <Wrapper>
            { isEmpty ? <EmptyCart /> : <FilledCart cartList={cartList} />}
        </Wrapper>
    );
}

export default Cart;