import React from 'react';
import styled from 'styled-components'
const Buttons = styled.button`
    padding: 10px;
    margin-top: 10px;
    font-size: 16px;
    text-transform: uppercase;
    background: #fff;
    border: 2px solid #333;
    border-radius: 1px;
    transition: all 0.3s;  
    &:hover {
        opacity: 0.7;
    } 
`
function Button(props) {
    return (
        <Buttons style={props.style} onClick={props.onClick} >{props.name}</Buttons>
    );
}

export default Button