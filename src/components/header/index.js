import React from 'react'
import styled from 'styled-components'
import { Link, useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'

const Wrapper = styled.div`
    display: flex;
    width: 100%;
    background-color: #eee;
    justify-content: space-around;
    
`
const Logo = styled.div`
    font-size: 50px;
    text-decoration: none;
    margin-left: 30%
`
const styleLogo = {
    textDecoration: 'none',
    color: '#000'
}
const Cart = styled.div`
text-align: center;
    width: 200px;
    &:hover {
        opacity: 0.6;
    }
`
const IconCart = styled.i`
    font-size: 50px;
`
function Header({ props }) {
    const cartList = useSelector(state => state.cart)
    return (
        <Wrapper>
            <Logo><Link style={styleLogo} to='/'>SHOP</Link></Logo>
            {useLocation().pathname !== '/cart' ?
                <Cart><Link style={styleLogo} to='/cart' ><IconCart className='fa fa-cart-plus'></IconCart> Cart {`(${cartList.length})`}</Link></Cart> : <Cart></Cart>}
        </Wrapper>
    );
}

export default Header