import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import DetailProduct from '../containers/detail-products/index'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    padding: 10px;
    &:hover {
        cursor: pointer;
      }
    // background-color: rgba(3, 165, 252, 0.4);
    border-radius: 5px;
    box-shadow: 0 1px 4px 0 rgb(0 0 0 / 14%);
    margin: 10px;
    background-color: #fff;


`
const styleLink = {
    textDecoration: 'none',
    textAlign: 'center',
    color: '#333',
    fontFamily: `"Times New Roman",Times,serif`
}

const ImageProduct = styled.img`
    width: 300px;
    height: auto;
    border-radius: 5px;

`
const NameProduct = styled.p`
    margin: 0;
`
const PriceProduct = styled.p`
    margin: 0;
`
function Product({ products }) {
    const { id, name, price, src } = products
    return (
        <Wrapper>
            <Link style={styleLink} to={`/detail-product/${id}`}>
                <ImageProduct src={src} ></ImageProduct>
                <NameProduct>{name}</NameProduct>
                <PriceProduct>${price}</PriceProduct>
            </Link>
        </Wrapper>
    );
}

export default Product;