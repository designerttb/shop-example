import { combineReducers, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import createSagaMiddleware from 'redux-saga';
import { watcherSaga } from "./redux/rootSaga";
import cartReducer from './slice/cartSlice';
import productReducer from './slice/productSlice';
const sagaMiddleware = createSagaMiddleware()

const rootReducer = combineReducers({
    cart: cartReducer,
    product: productReducer
})


const persistConfig = {
    key: 'root',
    storage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = configureStore({
    reducer: persistedReducer,
    middleware: [...getDefaultMiddleware({ thunk: false }), sagaMiddleware]
})

sagaMiddleware.run(watcherSaga)

export const persistor = persistStore(store)
export default store